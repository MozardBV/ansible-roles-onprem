<?php
 /*********************************************************************************************************************
  * Naam   :  modphplsql.php
  * Doel   :  php module ter vervanging van mod_plsql
  * Versie :  3.1 - 08-mei-2017
  *
  * Wannneer     Wie  Wijziging
  * -----------  ---  -------------------------------------------------------------------------------------------------
  * 26-dec-2013  HCU  Geschikt gemaakt voor nginx
  * 23-mei-2014  HCU  - raise in exception blok toegevoegd
  *                   - debug-modus toegevoegd
  *                   - probleem met "?" in documentnaam verholpen
  * 05-jun-2014  HCU  QUERY_STRING werd niet mee doorgegeven
  * 02-jul-2014  HCU  Genrieke code verplaats naar ../../modphplsql.php
  * 25-aug-2014  HCU  Zaak 29765 - Zaak 29639 - bevinding OVER - NOP 3 wijzigen naam deelzaak - 5.2.3
  *                   - params met : in de naam zijn wel toegestaan
  * 04-sep-2014  HCU  304 toegevoegd
  * 08-sep-2014  HCU  - 304 afgesterd, is bug in firefox 32!
  *                   - isset() om PATH_INFO en DOCUMENT_URI
  * 19-sep-2014  HCU  - NOP_6 toegevoegd: geen connectie met DB = 404!
  * 22-sep-2014  HCU  - NOP_6 -> do503 ipv 404!
  * 02-apr-2015  HCU  Username/inlogcode toegevoegd
  *   -mei-2015  HCU  Microtime moet met een true
  * 10-jun-2015  HCU  CGI verbetering
  *    sep-2015  HCU  PlsqlPathAlias toegevoegd
  * 13-okt-2015  HCU  Foutje hersteld in $debug gebruik en in PlsqlPathAlias
  * 04-nov-2015  HCU  - Strict bindings
  *                   - file-upload-errors niet negeren ;)
  *           ?  HCU  file-upload mogelijkheid met BFILEs toegevoegd
  * 11-apr-2016  HCU  urldecode om REQUEST_URI
  *   -mei-2016  HCU  foutje met headers hersteld
  * 30-nov-2016  HCU  soapEndpoints
  *                   timezone
  * 08-mei-2017  HCU  Merge met prod versie
  * 27-sep-2017  PGO  503 naar soft location
  * 28-sep-2017  HCU  bind var moet ook echt var zijn, geen functie
  * 21-feb-2018  HCU  fast-cgi-aanpassing: voortaan request_uri gebruiken ipv document_pad
  * 21-feb-2018  HCU  ipv apache note output header
  * 22-feb-2018  HCU  geen unlink wanneer $debug true
  * 26-feb-2018  HCU  apache_headers toch nog weer net anders
  * 26-feb-2018  HCU  undefine variable "protocol" in do503
  * 13-apr-2018  HCU  soap endpoints werkten niet zoals het hoorde
  *
  ***** .htaccess wordt dan:
  *
  *RewriteEngine On
  *
  *voor windows php met fast cgi toevoegen:
  *SetEnvIf Authorization .+ HTTP_AUTHORIZATION=$0
  *RewriteCond %{REQUEST_FILENAME} !-f
  *RewriteCond %{REQUEST_FILENAME} !-d
  *RewriteRule ^(.*)$ index.php [L]
  *
  * Laatste regel was:
  *
  *RewriteRule ^(.*)$ index.php/$1 [L]
  *
  *
  ********************************************************************************************************************/

#error_reporting(-1);
#ini_set('display_errors', 'On');

  date_default_timezone_set('Europe/Amsterdam');
  $startmicrotime=microtime(true);
  $timings['start']=microtime(true)-$startmicrotime;

 //if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && (!isset($_SERVER['HTTP_CACHE_CONTROL'])))
  //{
  //  header("HTTP/1.1 304 Not Modified",true,304);
  //  exit;
  //}

  $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";


  $cgi_env = array
    ( 'GATEWAY_INTERFACE'
    , 'HTTP_ACCEPT'
    , 'HTTP_ACCEPT_ENCODING'
    , 'HTTP_ACCEPT_LANGUAGE'
    , 'HTTP_COOKIE'
    , 'HTTP_AUTHORIZATION'
    , 'HTTP_IF_MODIFIED_SINCE'
    , 'HTTP_HOST'
    , 'HTTP_USER_AGENT'
    , 'REMOTE_ADDR'
    , 'REQUEST_CHARSET'
    , 'REQUEST_METHOD'
    //, 'REQUEST_PROTOCOL'
    , 'REMOTE_USER'
    , 'SCRIPT_NAME'
    , 'SERVER_NAME'
    , 'SERVER_PORT'
    , 'SERVER_PROTOCOL'
    , 'SERVER_SOFTWARE'
    , 'WEB_AUTHENT_PREFIX'
    , 'QUERY_STRING'
    );

  function do404($txt){
    global $debug, $errorDocs, $protocol;
    if ($debug)
    {
      print "<pre>".$txt." \n\n";
      print_r($GLOBALS);
    }
    else
    {
      if ($errorDocs[404])
      header('Location: '. $protocol.$_SERVER["HTTP_HOST"].$errorDocs[404]);
      else
      {
        header('HTTP/1.0 404 Not Found');
        echo "<h1>404 Not Found</h1>";
        echo "<p>The page that you have requested could not be found.</p>";
        echo "<p>".$txt."</p>";
      }
    }
    exit();
  }

  function do503($txt){
    global $debug, $errorDocs, $protocol;
    if ($debug)
    {
      print "<pre>".$txt." \n\n";
      print_r($GLOBALS);
    }
    else
    {
      header('HTTP/1.0 503 Not Found', true, 503 );
      header( "Retry-After: 3600" );
      header('Location: '. $protocol.$_SERVER["HTTP_HOST"].$errorDocs[503]);
      exit();
      echo "<h1>503 Service temporarily not available</h1>";
      echo "<p>Beste Mozard gebruiker</p>";
      echo "<p></p>";
      echo "<p>De server van de pagina die je bezoekt is momenteel niet beschikbaar vanwege onderhoud of vanwege een storing. </p>";
      echo "<p></p>";
      echo "<p>Wij doen er alles aan om de pagina zo spoedig mogelijk beschikbaar te maken.</p>";
      echo "<p></p>";
      echo "<p>Excuses voor het ongemak</p>";
      echo "<p></p>";
      echo "<p>Het Mozard team</p>";
      echo "<p></p>";
      echo "<p></p>";
      echo "<p>".$txt."</p>";
    }
    exit();
  }

function fileUploadCodeToMessage($code)
{
    switch ($code) {
        case UPLOAD_ERR_INI_SIZE:
            $message = "The uploaded file exceeds the upload_max_filesize directive in php.ini";
            break;
        case UPLOAD_ERR_FORM_SIZE:
            $message = "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
            break;
        case UPLOAD_ERR_PARTIAL:
            $message = "The uploaded file was only partially uploaded";
            break;
        case UPLOAD_ERR_NO_FILE:
            $message = "No file was uploaded";
            break;
        case UPLOAD_ERR_NO_TMP_DIR:
            $message = "Missing a temporary folder";
            break;
        case UPLOAD_ERR_CANT_WRITE:
            $message = "Failed to write file to disk";
            break;
        case UPLOAD_ERR_EXTENSION:
            $message = "File upload stopped by extension";
            break;

        default:
            $message = "Unknown upload error";
            break;
    }
    return $message;
}

if( !function_exists('apache_request_headers') ) {
  function apache_request_headers() {
    $arh = array();
    $rx_http = '/\AHTTP_/';
    foreach($_SERVER as $key => $val) {
      if( preg_match($rx_http, $key) ) {
        $arh_key = preg_replace($rx_http, '', $key);
        $rx_matches = array();
        // do some nasty string manipulations to restore the original letter case
        // this should work in most cases
        $rx_matches = explode('_', $arh_key);
        if( count($rx_matches) > 0 and strlen($arh_key) > 2 ) {
          //$arh[$arh_key] = $val; // ook de ongewijzigde toevoegen!
          foreach($rx_matches as $ak_key => $ak_val) $rx_matches[$ak_key] = ucfirst(strtolower($ak_val));
          $arh_key = implode('-', $rx_matches);
        }
        $arh[$arh_key] = $val;
      }
    }
    return( $arh );
  }
}


  $path_info_tmp=(isset($_SERVER["REQUEST_URI"]))?$_SERVER["REQUEST_URI"]:"";
  $path_info_a = explode( "/" ,$path_info_tmp );
  //print_r($path_info_a);
  if (count($path_info_a)>1)  {
     //echo $path_info_a[0];
     $path_info_a[0]= "";
     if ($path_info_a[1]==$dadname) $path_info_a[1]="";
     $path_info = "/".ltrim(implode("/",$path_info_a),"/");
    if (ltrim($path_info,"/")==="") $path_info="";
  }
  $path_info_a = explode( "?" ,$path_info );
  if (count($path_info_a)>1) $path_info = $path_info_a[0];

  // get GET and POST data
  // mod_plsql doet niet aan array's, toch kun je ze wel gebruiken.
  $na = array();$va = array();
  foreach($_GET  as $key => $value)
  {
    if (is_array($value))
    {
      for ($i=0;$i<count($value);$i++)
      {
        $na[count($na)]=$key."[]" ;
        $va[count($va)]=$value[$i];
      }
    }
    else
    {
      $na[count($na)]=$key;
      $va[count($va)]=$value;
    }
  }
  foreach($_POST as $key => $value)
  {
    if (is_array($value))
    {
      for ($i=0;$i<count($value);$i++)
      {
        $na[count($na)]=$key."[]" ;
        $va[count($va)]=$value[$i];
      }
    }
    else
    {
      $na[count($na)]=$key;
      $va[count($va)]=$value;
    }
  }
  //$postdata = file_get_contents("php://input");
  //foreach(explode('&',$postdata) as $key => $value)
  //  {
  //    $nv=explode('=',$value,2) ;
  //    if (!empty($nv[0])) { $na[count($na)]=urldecode($nv[0]); $va[count($va)]=urldecode($nv[1]); }
  //  }

  //check params for invalid chars
  for ($i=0;$i<count($na);$i++)  {
    $check=preg_match('/^([A-Za-z0-9_\[\].:-])*$/' ,$na[$i]);
    if ($check!==1) { do404( "NOP_3: invalid params" ); exit; }
  }

  //print '<pre>';print_r($GLOBALS);exit;

  // set cgi_env
  $ap_headers = apache_request_headers();
  $cgi_na = array
    ( 'REQUEST_IANA_CHARSET'
    , 'SCRIPT_URL'
    , 'PLSQL_GATEWAY'
    , 'GATEWAY_IVERSION'
    , 'DOCUMENT_TABLE'
    , 'DOC_ACCESS_PATH'
    , 'REQUEST_PROTOCOL'
    );
  $cgi_va = array
    ( 'UTF-8'
    , $_SERVER["REQUEST_URI"]
    , 'PHP'
    , '2'
    , $PlsqlDocumentTablename
    , 'document'
    , 'HTTP'
    );
  foreach ($cgi_env as $env) {
    $cgi_na[count($cgi_na)] = $env;
    if (isset($_SERVER[$env])) $cgi_va[count($cgi_va)] =  $_SERVER[$env];
    elseif (isset($ap_headers[$env])) $cgi_va[count($cgi_va)] =  $ap_headers[$env];
    else  $cgi_va[count($cgi_va)]=null;
  }
  foreach ($cgi_env_ap as $env) {
    $cgi_na[count($cgi_na)] = $env;
    if (isset($ap_headers[$env])) $cgi_va[count($cgi_va)] =  $ap_headers[$env];
    elseif (isset($_SERVER[$env])) $cgi_va[count($cgi_va)] =  $_SERVER[$env];
    else  $cgi_va[count($cgi_va)]=null;
  }
  if (isset($ap_headers['Authorization'])) {
    $cgi_na[count($cgi_na)] = 'HTTP_AUTHORIZATION';
    $cgi_va[count($cgi_va)] = $ap_headers['Authorization'];
  }
  $bDocProc = false;
  $bSoapCall = false;
  if (!empty($path_info)) {
    // default path = procname
    $script = $path_info;
    $requri = urldecode($_SERVER["REQUEST_URI"]);
    // documentprocedure
    if (substr($script,0,strlen($PlsqlDocumentPath)+2)=='/'.$PlsqlDocumentPath.'/') {
      $script = '/'.$PlsqlDocumentProcedure;
      $padinfo= substr( $requri,strpos($requri,$PlsqlDocumentPath)+strlen($PlsqlDocumentPath));
      //$padinfo= substr( $requri,strpos($requri,$PlsqlDocumentPath)-1);
      $bDocProc = true;
    }
    // pathalias, in mod_plsql max 1, here an array of as much as you like
    foreach($PlsqlPathAlias as $aliasNr => $aliasName) {
      if (substr($script,0,strlen($aliasName)+2)=='/'.$aliasName.'/') {
        $script = '/'.$PlsqlPathAliasProcedure[$aliasNr];
        //$padinfo= substr( $requri,strpos($requri,$aliasName)+strlen($aliasName));
        $padinfo= substr( $requri,strpos($requri,$aliasName)-1);
        $bDocProc = true;
      }
    }
    // endpoints, not possible in mod_plsql!!! alleen icm bFile
    if ($useBFile)
    foreach($soapEndPoint as $endpointNr => $endpointName) {
      if (substr($script,0,strlen($endpointName)+2)=='/'.$endpointName.'/') {
        $script = '/!'.$soapEndPointProcedure[$endpointNr];
        //$padinfo= substr( $requri,strpos($requri,$aliasName)+strlen($aliasName));
        $padinfo= substr( $requri,strpos($requri,$endpointName)-1);
        $bDocProc = true;
        //ok found enpoint, save complete request body for upload as file....
        $entityBody = file_get_contents('php://input');
        if (strlen($entityBody)>0){
          $fname = $bfileUploadPath.'/tmp_endpoint_'.$endpointName.'_'.mt_rand(1e5,1e6-1) .date('_Ymd_His').'_'. fmod(microtime(true), 1).'.blob';
          file_put_contents($fname, $entityBody);
          $bSoapCall = true;
        }
      }
    }

  }
  else {
    // no path: startpage
    $script = '/'.$startPage;
  }
  $bNaVa = (substr($script,0,2)=='/!');

  $bParams = (count($na)!=0);
  if ($bDocProc) {
    $cgi_na[count($cgi_na)] = 'PATH_INFO';
    //$cgi_va[count($cgi_va)] = substr($path_info,strlen($PlsqlDocumentPath)+1);
    $cgi_va[count($cgi_va)] = $padinfo;
    $na = array();$va = array();
  }
  else
  {
    $cgi_na[count($cgi_na)] = 'PATH_INFO';
    $cgi_va[count($cgi_va)] = $script;
  }
  // Change scriptname for the pl/sql call
  $script = substr($script,($bNaVa)?2:1);
  $bIsHeader=true;

  //print($script);

  //check scriptname for invalid chars
  $check=preg_match('/^([A-Za-z0-9._])*$/' ,$script);
  if ($check!==1) { do404( "NOP_4: invalid scriptname" );  exit; }
  // check scriptname first character = alpha
  $check=preg_match('/^[A-Za-z]/' ,$script);
  if ($check!==1) { do404( "NOP_5: invalid scriptname" ); exit; }

  $timings['preconnect']=microtime(true)-$startmicrotime;
  $conn = mozardconnect();
  $timings['postconnect']=microtime(true)-$startmicrotime;
  if (!$conn) { $m = oci_error(); do503("NOP_6: The service is temporarily unavailable" ); exit; }
  // alter session en reset packages
  $statement = oci_parse($conn,'alter session set NLS_LANGUAGE=\''.$NLS_lang.'\'' ); oci_execute($statement);oci_free_statement($statement);
  $statement = oci_parse($conn,'alter session set NLS_TERRITORY=\''.$NLS_territory.'\'' ); oci_execute($statement);oci_free_statement($statement);
  $statement = oci_parse($conn,'begin DBMS_SESSION.MODIFY_PACKAGE_STATE(1); end;' ); oci_execute($statement);oci_free_statement($statement);
  $timings['postalterses']=microtime(true)-$startmicrotime;
  // init cgi env
  $statement = oci_parse($conn,'begin owa.init_cgi_env( :cgi_i, :cgi_na, :cgi_va ); end;' );
  $timings['postparse']=microtime(true)-$startmicrotime;
  $aantal_na=count($cgi_na);
  $aantal_va=count($cgi_va);
  oci_bind_array_by_name($statement, ":cgi_na", $cgi_na, $aantal_na, -1, SQLT_CHR);
  oci_bind_array_by_name($statement, ":cgi_va", $cgi_va, $aantal_va, -1, SQLT_CHR);
  oci_bind_by_name($statement, ":cgi_i", $aantal_na);
  oci_execute($statement);
  oci_free_statement($statement);
  $timings['endcgi']=microtime(true)-$startmicrotime;
  // uploaded files naar de blobtable.:
  $filecount=0;
  if (count($_FILES)>0) {
    //print '<pre>';print_r($_FILES);print "\n";
    foreach ($_FILES as $key => $_file) {
      if ($_file['error']==0) {
        $fname = 'm'.microtime(true).'/'.$_file['name'];
        // files oppikken en in de document-tabel inserten...
        $filecount++;
        $timings['file-tmp-'. $filecount]=$_file['tmp_name'];

        if (!$useBFile)
        {

          $timings['file-pre-'. $filecount]=microtime(true)-$startmicrotime;
          $timings['file-tmp-'. $filecount]=$_file['tmp_name'];
          $blob = oci_new_descriptor($conn, OCI_DTYPE_LOB);
          $sql = "insert into ".$PlsqlDocumentTablename."
 (name,mime_type,doc_size, format, content_type, blob_content, dad_charset, last_updated)
 values (:nam,:mim,:siz,'BINARY','BLOB',empty_blob(),'ascii',sysdate) returning blob_content into :blb" ;
          $statement = oci_parse($conn,$sql);
          oci_bind_by_name($statement, ":blb" , $blob, -1, SQLT_BLOB);
          oci_bind_by_name($statement, ":nam" , $fname, 256, SQLT_CHR);
          oci_bind_by_name($statement, ":mim" , $_file['type'], 256, SQLT_CHR);
          oci_bind_by_name($statement, ":siz" , $_file['size'], 38, SQLT_INT);
          oci_execute($statement, OCI_DEFAULT);
          $blob->savefile($_file['tmp_name']);
          //oci_commit($conn);
          $blob->free();
          oci_free_statement($statement);
          $timings['file-post-'. $filecount]=microtime(true)-$startmicrotime;

        } else {

          move_uploaded_file($_file['tmp_name'] , $bfileUploadPath."/".basename($_file['tmp_name']));
          $timings['file-pre-'. $filecount]=microtime(true)-$startmicrotime;
          $sql = "insert into ".$PlsqlDocumentTablename."
 (name,mime_type,doc_size, format, content_type, blob_content, dad_charset, last_updated)
 values (:nam,:mim,:siz,'BINARY','BLOB', " . $bfileProcedure . "(:fname),'ascii',sysdate) " ;
	        $statement = oci_parse($conn,$sql);
	        $bfile_tmp_name = basename($_file['tmp_name']);
          oci_bind_by_name($statement, ":nam" , $fname, 256, SQLT_CHR);
          oci_bind_by_name($statement, ":mim" , $_file['type'], 256, SQLT_CHR);
          oci_bind_by_name($statement, ":fname" ,$bfile_tmp_name, 256, SQLT_CHR);
          oci_bind_by_name($statement, ":siz" , $_file['size'], 38, SQLT_INT);
          oci_execute($statement);
          oci_free_statement($statement);
          $timings['file-post-'. $filecount]=microtime(true)-$startmicrotime;
          if (!$debug) unlink($bfileUploadPath."/".basename($_file['tmp_name']));

        }



        //print_r ($na);print"\n";print_r ($va);print"\n";
        // en toevoegen aan nava
        $na[count($na)]=$key; $va[count($va)]=$fname;
        //print_r ($na);print"\n";print_r ($va);print"\n";
      } else  if ($_file['error']!=4)  error_log('File upload error: '.$_file['error'].': '.fileUploadCodeToMessage($_file['error']) );
    }
  }

  // soapcall -> fileupload!
  if ($bSoapCall) {
          $filecount++;
          $timings['file-pre-'. $filecount]=microtime(true)-$startmicrotime;
          $sql = "insert into ".$PlsqlDocumentTablename."
 (name,mime_type,doc_size, format, content_type, blob_content, dad_charset, last_updated)
 values (:nam,:mim,:siz,'BINARY','BLOB', " . $bfileProcedure . "(:fname),'ascii',sysdate) " ;
          $statement = oci_parse($conn,$sql);
          $mime = 'application/octet-stream';
          $bfile_tmp_name = basename($fname);
          $entityBodyLen = strlen($entityBody);
	        oci_bind_by_name($statement, ":nam" , $bfile_tmp_name, 256, SQLT_CHR);
          oci_bind_by_name($statement, ":mim" , $mime, 256, SQLT_CHR);
          oci_bind_by_name($statement, ":fname" , $bfile_tmp_name, 256, SQLT_CHR);
          oci_bind_by_name($statement, ":siz" , $entityBodyLen, 38, SQLT_INT);
          oci_execute($statement);
          oci_free_statement($statement);
          $timings['file-post-'. $filecount]=microtime(true)-$startmicrotime;
          if (!$debug) unlink($fname);
          $na[count($na)]='entityBody'; $va[count($va)]=basename($fname);

  }

  // build bind params string
  if ($bNaVa) $params = '(name_array=>:na,value_array=>:va)'; else if ($bParams) {
    //$params = '(:'.implode(',:',$na).')' ;
    $params = '(';
    for ($i=0;$i<count($na);$i++) $params .= (($i==0)?'':',').$na[$i].'=>:'.$na[$i] ;
    $params .= ')';
    //echo $params;
  } else
    $params = ' ' ;
  // build plsql block
  $plsql = "declare sqlerrm__ varchar2(4000); rc__ integer; simple_list__ owa_util.vc_arr; complex_list__ owa_util.vc_arr;
begin simple_list__(1) := 'sys.%'; simple_list__(2) := 'dbms\_%'; simple_list__(3) := 'utl\_%';
simple_list__(4) := 'owa\_%'; simple_list__(5) := 'owa.%'; simple_list__(6) := 'htp.%';
simple_list__(7) := 'htf.%'; simple_list__(8) := 'wpg_docload.%';
if ((owa_match.match_pattern(:scriptname, simple_list__, complex_list__, true))) then rc__ := 2;
else begin htp.htbuf_len := 63; ".$script.$params."; if (wpg_docload.is_file_download) then rc__ := 1;
wpg_docload.get_download_file (:docinfo); commit; else rc__ := 0; commit; owa.get_page (:thepage, :irows);
end if; exception when others then rc__ := 3; raise; end; end if; :rc := rc__; end;";
  $plsql = " declare
  sqlerrm__      varchar2(4000);
  app_user__     varchar2(4000);
  rc__           integer;
  simple_list__  owa_util.vc_arr;
  complex_list__ owa_util.vc_arr;
begin
  simple_list__(1) := 'sys.%';
  simple_list__(2) := 'dbms\_%';
  simple_list__(3) := 'utl\_%';
  simple_list__(4) := 'owa\_%';
  simple_list__(5) := 'owa.%';
  simple_list__(6) := 'htp.%';
  simple_list__(7) := 'htf.%';
  simple_list__(8) := 'wpg_docload.%';
  if ((owa_match.match_pattern(:scriptname, simple_list__, complex_list__, true)))
  then
    rc__ := 2;
  else
    begin
      htp.htbuf_len := 63;
      ".$script.$params.";
      app_user__ := get_inlogcode;
      if (wpg_docload.is_file_download)
      then
        rc__ := 1;
        wpg_docload.get_download_file (:docinfo);
        commit;
      else
        rc__ := 0;
        commit;
        owa.get_page (:thepage, :irows);
      end if;
    exception
      when others then
        rc__ := 3;
        raise;
    end;
  end if;
  :rc := rc__;
  :app_user := app_user__;
end;" ;
  $timings['preparse2']=microtime(true)-$startmicrotime;
  $statement = oci_parse($conn,$plsql );
  $timings['postparse2']=microtime(true)-$startmicrotime;
  $resultcode =0;
  $docinfo='';
  $thepage = array(" ","empty-page" );
  $irows = 99999999;
  if ($bNaVa) {
    if (count($na)==0) {$na[0]="";$va[0]="";} // dit is een hack :(
    oci_bind_array_by_name($statement, ":na", $na, count($na), -1, SQLT_CHR);
    oci_bind_array_by_name($statement, ":va", $va, count($va), -1, SQLT_CHR);
  } else {
    if ($bParams) for ($i=0;$i<count($na);$i++) oci_bind_by_name($statement, ':'.$na[$i], $va[$i], 32767, SQLT_CHR);
  }


  oci_bind_array_by_name($statement, ":thepage", $thepage, 100000, 255, SQLT_CHR);
  oci_bind_by_name($statement, ":irows"        , $irows, 38, SQLT_INT);
  oci_bind_by_name($statement, ":rc"           , $resultcode, 2, SQLT_INT);
  oci_bind_by_name($statement, ":app_user"     , $app_user, 256, SQLT_CHR);
  oci_bind_by_name($statement, ":scriptname"   , $script, 256, SQLT_CHR);
  oci_bind_by_name($statement, ":docinfo"      , $docinfo, 256, SQLT_CHR);
  $timings['preexec2']=microtime(true)-$startmicrotime;
  $oci_rslt = oci_execute($statement);
  if (!$oci_rslt) $err = oci_error($statement);
  $timings['prefree2']=microtime(true)-$startmicrotime;
  oci_free_statement($statement);
  $timings['postfree2']=microtime(true)-$startmicrotime;

  if (!$oci_rslt)
  {
    do404("NOP_0: CALL ERROR");//OCI ERROR!
  }
  elseif ($resultcode==0)
  {
    header( 'X-Moz-Username: '. $app_user );
    if ($irows==99999999)
    {
      do404("NOP_1: Does not compute");
    } else
    {
      $ii=0;
      $buff = null;
      //$bIsHeader = false;

      //print "\n" ;      print "\n" ;      print "\n<pre>" ;      print_r($GLOBALS);      exit;

      $prevline="" ;
      foreach ($thepage as $line)
      {
        // om de pagina in debug mode (raw data vanaf oracle) te zien, de volgende regel insterren
        //$ii++;if($ii==1)print "modphplsql debug mode!!--!!" ;  print "..".$ii."_:_". $line;
        if ($ii==0)
        if ($bIsHeader)
        {
          $buff.=$line;
          if (strpos($buff,"\n")===false) { } else {
            // strip de achterlijke x-oracle-ignore en de verkeerde content-length...
            // php doet het beter!..
            if (($buff!='X-ORACLE-IGNORE: IGNORE'."\n") && (strpos($buff,'Content-length: ')===false))
            {

              if (substr($buff,0,7)=='Status:') {
                $rsltcode_a=explode ( " "  , $buff );
                $rsltcode_c=$rsltcode_a[1];
                $buff=implode(" ",$rsltcode_a);
                header($buff, true, $rsltcode_c);
              }
              else header($buff);
            }
            $buff=null;
          }
        } else {
          print $line;
        }
        //if ($bIsHeader && strlen($line)==1) {$bIsHeader=false; } //print " \n" ; header('Vary:');
        if ($bIsHeader && strlen($line)==1 && strlen(trim($prevline))!=63) { $bIsHeader=false; }
        $prevline=$line;
      }
      if ($bIsHeader) print "\n" ;

    }
  } elseif ($docinfo=='B')
  {
    header( 'X-Moz-Username: '. $app_user );
    $plsql = "declare thepage htp.htbuf_arr; begin
      owa.get_page (thepage, :irows); :thepage := thepage;
      wpg_docload.get_download_blob(p_blob=>:blb);
      commit; end;";
    $statement = oci_parse($conn,$plsql );
    $blob = oci_new_descriptor($conn, OCI_DTYPE_LOB);
    oci_bind_by_name($statement, ":blb"  , $blob, -1, SQLT_BLOB);
    oci_bind_by_name($statement, ":irows"   , $irows, 38, SQLT_INT);
    oci_bind_array_by_name($statement, ":thepage", $thepage, 32767, 1024, SQLT_CHR);
    oci_execute($statement);
    $buff = null;

    //echo " aaaa " ;
    //print_r($thepage);
    //echo " aaaa " ;

    $prevline="" ;
    foreach ($thepage as $line)
    {
      // om de pagina in debug mode (raw data vanaf oracle) te zien, de volgende regel insterren
      // $ii++;if($ii==1)print "modphplsql debug mode!!--!!\n" ;  print "..".$ii."_:_". (($bIsHeader)?"hdr":"lin") . " " . strlen($line) . "  " . trim($line). "\n"  ;

      if ($bIsHeader)
      {
        $buff.=$line;
        if (strpos($buff,"\n")===false) { } else {
        // strip de achterlijke x-oracle-ignore en de verkeerde content-length...
        // php doet het beter!..
        if (($buff!='X-ORACLE-IGNORE: IGNORE'."\n") && (strpos($buff,'Content-length: ')===false)) header ($buff);
        $buff=null;
        }
      } else {
        print $line;
      }
      if ($bIsHeader && strlen($line)==1 && strlen(trim($prevline))!=63) { $bIsHeader=false; }
      $prevline=$line;
    }
    print $blob->read($blob->size());
    oci_free_descriptor($blob);
    oci_free_statement($statement);
  } else
  {
    do404("NOP_2: Does not compute");
  }

  // Close Oracle DB connectie
  oci_close($conn);
  $timings['gereed']=microtime(true)-$startmicrotime;

  //print_r($timings);
  //if ($thepage[1]=='Error!') print_r($GLOBALS);
?>
