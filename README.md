# mozardbv/ansible-roles-onprem

Infrastructure-as-Code definities voor Mozard on-premise

- [Meld een bevinding](https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=367)
- [Verzoek nieuwe functionaliteit](https://intranet.mozard.nl/mozard/!suite09.scherm1089?mWfr=604&mDdv=990842)

## Inhoudsopgave

- [mozardbv/ansible-roles-onprem](#mozardbvansible-roles-onprem)
  - [Inhoudsopgave](#inhoudsopgave)
  - [Over dit project](#over-dit-project)
  - [Aan de slag](#aan-de-slag)
    - [Afhankelijkheden](#afhankelijkheden)
    - [Installatie](#installatie)
  - [Tests draaien](#tests-draaien)
    - [Coding style tests](#coding-style-tests)
  - [Deployment](#deployment)
  - [Gebouwd met](#gebouwd-met)
  - [Bijdragen](#bijdragen)
  - [Versioning](#versioning)
  - [Auteurs](#auteurs)
  - [Licentie](#licentie)

## Over dit project

## Aan de slag

### Afhankelijkheden

- `ansible` >= 2.10
- `git-lfs` >= 2.10
- `openssh`

Ansible werkt out-of-the-box niet op Microsoft Windows. Dat kan alleen door deze te gebruiken in Linux Subsystem for Windows.

### Installatie

Clone de git repository:

```sh
git clone https://gitlab.com/MozardBV/ansible-roles-onprem.git
```

Haal requirements binnen

```sh
python3 -m pip install -r requirements.txt
```

Zet mitogen als connection layer:

```sh
# Pad kan afwijken afhankelijk van de omgeving
export ANSIBLE_STRATEGY_PLUGINS=/usr/local/lib/python3.9/site-packages
export ANSIBLE_STRATEGY=mitogen_linear
```

## Tests draaien

### Coding style tests

Draai `ansible-lint` op een willekeurig playbook om deze te testen. Dit is altijd recursief, dus om de gehele repository te testen:

```sh
ansible-lint ansible/site.yml`
```

## Deployment

Vanaf de `main` branch:

```sh
ansible-playbook ansible/site.yml
```

Of, in plaats van `site.yml`, de rol van de servers die je wilt uitrollen.

Zie ook:

```sh
man ansible
```

## Gebouwd met

- [Ansible](https://www.ansible.com) - confuratiemanagementframework
- [ansible-lint](https://github.com/ansible/ansible-lint) - linter
- [Mitogen for Ansible](https://mitogen.networkgenomics.com/ansible_detailed.html) - connection layer

## Bijdragen

Zie [CONTRIBUTING.md](https://gitlab.com/MozardBV/ansible-roles-onprem/-/blob/main/CONTRIBUTING.md) voor de inhoudelijke procesafspraken.

## Versioning

Deze repository is een rolling release waarbij [main](https://gitlab.com/MozardBV/ansible-roles-onprem/-/tree/main) gelijk is aan de configuratie in Mozard SaaS productie.

## Auteurs

- **Patrick Godschalk (Mozard)** - _Maintainer_ - [pgodschalk](https://gitlab.com/pgodschalk)
- **Henrie Cuijpers (Cuijpers IT)** - _Maintainer_ - [hetOrakel](https://gitlab.com/hetOrakel)
- **Felix Kraneveld (Mozard)** - _Integratiespecialist_ - [felixkra](https://gitlab.com/felixkra)
- **Pascal van der Weiden (Sidion)** - _Linux beheerder_ - [pvanderweiden](https://gitlab.com/pascalvanderweiden)
- **André Hettinga (HetDB)** - _Oracle DBA_ - [andrehet](https://gitlab.com/andrehet)

Zie ook de lijst van [contributors](https://gitlab.com/MozardBV/ansible-roles-onprem/-/graphs/main) die hebben bijgedragen aan dit project.

## Licentie

[SPDX](https://spdx.org/licenses/) license: `UNLICENSED`

Copyright (c) 2006-2021 Mozard B.V. - Alle rechten voorbehouden.

[Leveringsvoorwaarden](https://www.mozard.nl/mozard/!suite86.scherm0325?mPag=204&mLok=1)
