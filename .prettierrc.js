module.exports = {
  printWidth: 120,
  tabWidth: 2,
  useTabs: false,
  singleQuote: false,
  quoteProps: "as-needed",
  bracketSpacing: true,
  proseWrap: "never",
  endOfLine: "lf",
};
